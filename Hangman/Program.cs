﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Cryptography.X509Certificates;

namespace Hangman
{
    class HangmanGame
    {
        static void Main(string[] args)
        {
            HangmanGame game = new HangmanGame();
            var word = game.GetDictionaryWord();
            var usedLetters = new List<String>();
            var view = game.InitHangman();
            var tries = 0;
            var win = false;
            var wordLine = new List<String>();
            for (var i = 0; i < word.Length; i++)
            {
                wordLine.Add("_");
            }
            while (tries < 7 && !win)
            {
                Console.Clear();
                // Update the console to show the current status of the hangman
                view = game.UpdateHangmanStatus(view, tries);

                // Display the used letters
                game.DisplayUsedLetters(usedLetters);

                // Update the console with the blanks or successful letters for the guess
                game.UpdateWordLine(wordLine);

                // Validate the user input
                var attempt = game.AuthenticateAttempt(Console.ReadLine());

                if (attempt != "")
                {
                    // does the word contain the attempted letter and have they already used it?
                    if (word.Contains(attempt) && !usedLetters.Contains(attempt))
                    {
                        Console.WriteLine("Letter [" + attempt + "] is in the word!");

                        // change the guess wordLine variable to show the letters successfully guessed
                        for (var i = 0; i < word.Length; i++)
                        {
                            if (word[i] == Convert.ToChar(attempt))
                            {
                                wordLine[i] = attempt;
                            }
                        }
                        if (game.PlayerWinCheck(word, wordLine)) win = true;
                    }

                    // Has the player used the letter already?
                    else if (usedLetters.Contains(attempt))
                    {
                        Console.WriteLine("You have used this word already!");

                        // Used continue as to not duplicate the word in the usedWords list
                        continue;
                    }
                    else
                    {
                        Console.WriteLine("Letter does not match");

                        // Failed attempt. Takes player one step closer to loss
                        tries++;
                    }
                    usedLetters.Add(attempt);
                }
                else
                {
                    Console.WriteLine("Input invalid. Please enter one letter...");

                }
                Console.ReadLine();
            }
            Console.Clear();
            game.UpdateHangmanStatus(view, tries);
            if (game.PlayerWinCheck(word, wordLine))
            {
                Console.WriteLine("Congratulations! You won!");
            }
            else
            {
                Console.WriteLine("Nope, he's dead. Better luck next time.");
            }
        }


        private bool PlayerWinCheck(string word, List<String> wordList)
        {
            for (var i = 0; i < word.Length; i++)
            {
                if (word[i] != Convert.ToChar(wordList[i]))
                {
                    return false;
                }
            }
            return true;
        }

        private void DisplayUsedLetters(List<String> usedLetterList)
        {
            Console.WriteLine("***************");
            Console.WriteLine(" Letters used:");
            var usedLetterLine = string.Join(" ", usedLetterList.Select(x => x));
            Console.WriteLine(usedLetterLine);
            Console.WriteLine("***************");
        }

        private string AuthenticateAttempt(string attempt)
        {
            if (attempt.Length == 1 && !attempt.All(char.IsDigit))
            {
                return attempt.ToLower();
            }
            return "";
        }

        private void UpdateWordLine(List<String> wordLine)
        {
            var line = "";
            for (var i = 0; i < wordLine.Count; i++)
            {
                line += wordLine[i] + " ";
            }
            Console.WriteLine("  " + line);
        }

        private string GetDictionaryWord()
        {
            Random rnd = new Random();
            string[] words = { "apple", "mango", "papaya", "banana", "guava", "pineapple" };
            return words[rnd.Next(0, words.Length)];
        }

        private string[,] InitHangman()
        {
            var view = new string[6,6];
            
            for (var i = 0; i < 6; i++)
            {
                for (var j = 0; j < 6; j++)
                {
                    view[i, j] = " ";
                }
            }

            return view;
        }

        private string[,] UpdateHangmanStatus(string[,] view, int tries = 0)
        {
            Header();

            switch (tries)
            {
                case 1:
                    for (var i = 0; i < 6; i++) view[5, i] = "-";
                    break;
                case 2:
                    for (var i = 1; i < 5; i++) view[i, 5] = "|";
                    break;
                case 3:
                    for (var i = 1; i < 5; i++) view[0, i] = "_";
                    break;
                case 4:
                    view[1, 1] = "|";
                    break;
                case 5:
                    view[2, 1] = "O";
                    break;
                case 6:
                    view[3, 1] = "+";
                    break;
                case 7:
                    view[4, 1] = "^";
                    break;
                default:
                    break;
            }
            for (var i = 0; i < 6; i++)
            {
                var line = "";
                for (var j = 0; j < 6; j++)
                {
                    line += view[i, j] + " ";
                }
                Console.WriteLine("  " + line);
            }
                return view;

        }

        public void Header()
        {
            Console.WriteLine("#################");
            Console.WriteLine("#### Hangman ####");
            Console.WriteLine("#################");
        }
    }
}
